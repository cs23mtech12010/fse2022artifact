About the Artifact
------------------
The artifact contains AndRaceR tool to analyse android
applications(.apk) for dataraces and redundant synchronisation blocks.

The artifact is available as Installation Package(IP) at,
https://drive.google.com/drive/folders/13aSR5ZS3czeNnaM9OXJQvX34s59iOz1Y?usp=sharing

Following are the details of the Virtual Machine to analyse using
AndRacer in the IP.
Name:                        FSE2022
Guest OS:                    Debian (64-bit)
Memory size:                 16384MB

Instuctions to Install
----------------------

Host OS : Ubuntu 20.04.4 LTS

1. Download and Install Virtual Box (versioned 6.1.34), for specified host.
Link : https://www.virtualbox.org/wiki/Download_Old_Builds_6_1

2. Download the Virtual Machine.
Link : https://drive.google.com/drive/folders/13aSR5ZS3czeNnaM9OXJQvX34s59iOz1Y?usp=sharing

3. Import the Virtual Machine into the Virtual Box.
Link : https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/ovf.html


Tool Usage
----------
1. open a bash shell in the terminal

2. change to the working directory
> cd $ANDRACER

3. run the datarace analysis
> make drace

4. View the results of the datarace analysis
> cd $RACE_RESULTS

5. run the redundant synchronisation analysis(RSA)
> make rsb

6. View the results of the RSA analysis
> cd $RSB_RESULTS


Artifact Variables
-----------------
Following are the useful Linux environment variables in the ~/.bashrc
of the Installation Package.

a. ANDROID, points to the directory containing the android sdk.

b. ANDRACER, points to the directory with Makefile to build and run
the analysis for all the benchmarks. This is the path at which the
terminal opens by default.

c. ANDRACER_SRC, points to the source directory of the tool resides.

d. BENCHMARKS, points to the directory containing all the benchmarks
to analyse.

e. RACE_RESULTS, points to the directory containing the results of the
datarace analysis as textfiles with names [application_name].txt.

f. RSB_RESULTS, points to the directory containing the results of the
redundant synchronisation analysis as textfiles with names
[application_name].txt.


Analysis Output Details
-----------------------

Following are the columns shown as output for analysis and description
of the columns.

Datarace Analysis
=================
The analysis detects conflicting accesses that may execute in parallel.

Thds, 	     number of abstract threads.
Tasks, 	     number of abstract tasks.
AR-EB, 	     number of executes-before relations detected by AndRacer.
AR-EB Time,  time to calculate executes-before relations.
CA,   	     number of conflicting accesses.
Syn, 	     number of conflicting accesses protected by locks.
EB, 	     number of conflicting accesses ordered by executes-before relation.
SEB, 	     number of conflicting accesses solely ordered by executes-before relation.
PR, 	     number of potential racy accesses.
HPR, 	     number of harmful potential racy accesses.

Redundant Synchronisation Analysis
==================================
The analysis detects synchronised blocks not protecting any
conflicting accesses.

Thds, 	    number of abstract threads.
Tasks, 	    number of abstract tasks.
CA, 	    number of conflicting accesses.
RSB, 	    number of redundant synchronisation blocks.
